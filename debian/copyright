Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: GLE
Source: :pserver:anonymous@mgl1.scripps.edu:2401/opt/cvs

Files: *
Copyright: 2008-2014 Linas Vepstas <linas@linas.org>
           and his employer IBM under the license stated below.
License: mgltools

Files: debian/*
Copyright: 2008-2014 Steffen Moeller <moeller@debian.org>
           2014 Thorsten Alteholz <debian@alteholz.de>
           2016 Andreas Tille <tille@debian.org>
License: mgltools

License: mgltools
 This software is copyrighted by Michel F. Sanner (sanner@scripps.edu) and TSRI.
 The following terms apply to all files associated with the software
 unless explicitly disclaimed in individual files.
 .
       MGLTOOLS SOFTWARE LICENSE AGREEMENT.
 .
  1. Grant Of Limited License; Software Use Restrictions. The programs
     received by you will be used only for NON COMMERCIAL purposes.
     This license is issued to you as an individual.
 .
     For COMMERCIAL use done with the software please contact Michel F.
     Sanner for details about commercial usage license agreements.
 .
     For any question regarding license agreements, please contact
             Michel Sanner:
             TSRI, Molecular Biology Department, TCP 26,
             10550 North Torrey Pines Road, La Jolla, CA 92037
             sanner@scripps.edu
             tel (858) 784-7742
             fax (858) 784-2341
 .
  2. COMMERCIAL USAGE is defined as revenues generating activities. These
     include using this software for consulting activities and selling
     applications built on top of, or using this software. Scientific
     research in an academic environment and teaching are considered
     NON COMMERCIAL.
 .
  3. Copying Restrictions. You will not sell or otherwise distribute commercially
     these programs or derivatives to any other party, whether with or without
     consideration.
 .
  4. Ownership of Software. You will not obtain, and will not attempt to
     obtain copyright coverage thereon without the express purpose written
     consent of The Scripps Research Institute and Dr. Sanner.
 .
  5. IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
     FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
     ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
     DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
     POSSIBILITY OF SUCH DAMAGE.
 .
  6. THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
     INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
     FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
     IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
     NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
     MODIFICATIONS.
 .
 .
    From src/COPYING.src
 .
 The following agreement applies to the source code in this directory
 only!  It does *NOT* apply to the HTML documentation or to the demo
 programs, or to other items distributed with this package!
 ---------------------------------------------------------------------
 .
 SOFTWARE AGREEMENT
 .
 PLEASE READ THIS AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THIS
 SOFTWARE.  IF YOU INSTALL OR USE THIS SOFTWARE, YOU AGREE TO THESE
 TERMS.
 .
 This software is owned by International Business Machines Corporation
 ("IBM"), or its subsidiaries or IBM's suppliers, and is copyrighted and
 licensed, not sold.  IBM retains title to the software, and grants you a
 nonexclusive license for the software.
 .
 Under this license, you may:
 .
 1) use the software on one or more machines at a time;
 2) make copies of the software for use or backup purposes within your
    enterprise;
 3) modify this software and merge it with another program; and
 4) make copies of the original file you downloaded and distribute it,
    provided that you transfer a copy of this license to the other party.
    The other party agrees to these terms by its first use of this software.
 .
 You must reproduce the copyright notice and any other legend of
 ownership on each copy or partial copy of the software.
 .
 This software, as provided by IBM, is only intended to assist in the
 development of a working software program. The software may not function
 as written: additional code is required. In addition, the software may
 not compile and/or bind successfully as written.
 .
 IBM PROVIDES THE SOFTWARE "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
 EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK
 AS TO THE QUALITY AND PERFORMANCE OF THE SOFTWARE IS WITH YOU.  SHOULD
 ANY PART OF THE SOFTWARE PROVE DEFECTIVE, YOU ASSUME THE ENTIRE COST OF
 ALL NECESSARY SERVICING, REPAIR OR CORRECTION. IN NO EVENT, UNLESS
 REQUIRED BY APPLICABLE LAW, SHALL IBM BE LIABLE TO YOU FOR DAMAGES,
 INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 ARISING OUT OF THE USE OR INABILITY TO USE THE SOFTWARE (INCLUDING BUT
 NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES
 SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE SOFTWARE TO
 OPERATE WITH ANY OTHER PROGRAMS), EVEN IF IBM HAS BEEN ADVISED IN
 ADVANCE OF THE POSSIBILITY OF SUCH DAMAGES.
 .
 IBM does not warrant that the contents of the software will meet your
 requirements, that the software is error-free or that the software does
 not infringe on any intellectual property rights of any third party.
 .
 IBM may make improvements and/or changes in the software at any time.
 .
 Changes may or may not be made periodically to the information in the
 software; these changes may be reported, for the software included
 herein, in new editions.
 .
 References, if any, in the software to IBM products, programs, or
 services do not imply that IBM intends to make these available in all
 countries in which IBM operates. Any reference to an IBM licensed
 program in the software is not intended to state or imply that only
 IBM's  licensed program may be used. Any functionally equivalent program
 may be used.
 .
 The laws of New York State govern this agreement.
 .
 In addition to the license above, the Debian project also got the explicit
 permission from the copyright owner to distribute the software:
  Date: Mon, 20 May 2013 10:07:13 -0700
  From: Michel Sanner <sanner@scripps.edu>
  To: Thorsten Alteholz <debian-med@alteholz.de>
  Cc: "annao >> Anna Omelchenko" <annao@scripps.edu>
  Subject: Re: mgltools license
 .
  Hello Thorsten
 .
  Sorry for not following up this earlier. We are delighted to have Debian
  include the MGLTools in their distribution.
  So you need us to amend the License text or will this email suffice ?
 .
  Thanks
 .
  --
 .
  -----------------------------------------------------------------------
     o
    /    Michel F. Sanner Ph.D.            The Scripps Research Institute
  o     Associate Professor               Department of Molecular Biology
    \                                      10550 North Torrey Pines Road
     o   Tel. (858) 784-7742               La Jolla, CA 92037, TPC 26
    /    Fax. (858) 784-2341
  o     sanner@scripps.edu                http://www.scripps.edu/~sanner
  -----------------------------------------------------------------------
